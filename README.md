# HT16K33

[![Version info](https://img.shields.io/crates/v/ht16k33-async.svg)](https://crates.io/crates/ht16k33-async)
[![Documentation](https://docs.rs/ht16k33-async/badge.svg)](https://docs.rs/ht16k33-async)
[![pipeline status](https://gitlab.com/nils-van-zuijlen/ht16k33-async/badges/master/pipeline.svg)](https://gitlab.com/nils-van-zuijlen/ht16k33-async/-/commits/master)

`ht16k33-async` is an async driver for the [Holtek HT16K33 "RAM Mapping 16\*8 LED Controller Driver with keyscan"](http://www.holtek.com/productdetail/-/vg/HT16K33).

Currently, only the 28-pin SOP package type is supported.

## Features

- [x] Uses the [`embedded-hal-async`](https://crates.io/crates/embedded-hal-async) hardware abstraction.
- [x] Supports `no_std` for embedded devices.
- [ ] Supports all 20/24/28-pin SOP package types.
- [x] Displays all 128 LEDs.
- [ ] Support dimming.
- [x] Reads keyscan.
- [ ] Manages interrupts.

## Examples

The examples are written using embassy for Raspberry Pi Pico.
As the Pico is a 3.3V device, you will need to use an I2C level shifter like the TCA9517 to talk to the HT16K33.

![](imgs/example-segments.jpg)

## Support

For questions, issues, feature requests, and other changes, please file an [issue in the gitlab project](https://gitlab.com/nils-van-zuijlen/ht16k33-async/-/issues).

## License

Licensed under the Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0).

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you,
as defined in the Apache-2.0 license, shall be licensed as above, without any additional terms or conditions.
