#![no_std]

//! # HT16K33
//!
//! `ht16k33-async` is an async driver for the [Holtek HT16K33 "RAM Mapping 16\*8 LED Controller Driver with keyscan"](http://www.holtek.com/productdetail/-/vg/HT16K33).
//!
//! Currently, only the 28-pin SOP package type is supported.
//!
//! ## Features
//!
//! - [x] Uses the [`embedded-hal-async`](https://crates.io/crates/embedded-hal-async) hardware abstraction.
//! - [x] Supports `no_std` for embedded devices.
//! - [ ] Supports all 20/24/28-pin SOP package types.
//! - [x] Displays all 128 LEDs.
//! - [ ] Support dimming.
//! - [x] Reads keyscan.
//! - [ ] Manages interrupts.
//!
//! ## Support
//!
//! For questions, issues, feature requests, and other changes, please file an [issue in the gitlab project](https://gitlab.com/nils-van-zuijlen/ht16k33-async/-/issues).
//!
//! ## Usage
//!
//! Using the [`embassy`]()
//! crate which implements the `embedded-hal-async` traits, on a Raspberry Pi Pico.
//! ```ignore
//! #![no_std]
//! #![no_main]
//!
//! use embassy_executor::Spawner;
//! use panic_halt as _;
//! use embassy_rp::{i2c, bind_interrupts};
//! use embassy_rp::peripherals::I2C1;
//!
//! bind_interrupts!(struct Irqs {
//!     I2C1_IRQ => i2c::InterruptHandler<I2C1>;
//! });
//!
//! #[embassy_executor::main]
//! async fn main(spawner: Spawner) {
//!     let p = embassy_rp::init(Default::default());
//!
//!     let scl = p.PIN_3;
//!     let sda = p.PIN_2;
//!     let i2c = i2c::I2c::new_async(p.I2C1, scl, sda, Irqs, i2c::Config::default());
//!     let mut driver = HT16K33::new(i2c, 0x3F);
//!
//!     driver.setup().await.unwrap();
//!
//!     let mut buffer = [0u8; 6];
//!
//!     loop {
//!         // Copy keypresses to LEDs
//!         driver.read_keys(&mut buffer).await.unwrap();
//!         driver.write_whole_display(&buffer).await.unwrap();
//!     }
//! }
//! ```


use embedded_hal_async as hal;

mod types;

pub use crate::types::{HT16K33, DisplayBlink, DisplayOnOff};
